package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemClientFB implements  ItemClient {
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {

        if(Math.random()<0.5){

            List<Item> list=new ArrayList<>();
            list.add(new Item(1,"缓存上sss皮",1));

            return  JsonResult.ok().data(list);
        }



        return JsonResult.err().msg("获取商品列表失败");
    }

    @Override
    public JsonResult<?> decreaseNuber(List<Item> items) {
        return JsonResult.err().msg("减少商品库存失败");
    }
}
