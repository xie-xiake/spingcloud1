package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserClientFB  implements UserClient{
    @Override
    public JsonResult<User> getUser(Integer userId) {
        // 模拟缓存数据
        if(Math.random()<0.5){

            List<Item> list=new ArrayList<>();
            list.add(new Item(1,"缓存上皮",1));

   return  JsonResult.ok().data(list);
        }


        return  JsonResult.err().msg("获取用户失败");
    }

    @Override
    public JsonResult<?> addScore(Integer userId, Integer score) {
        return JsonResult.err().msg("增加积分失败");
    }
}
